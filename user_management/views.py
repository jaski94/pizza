import datetime

from django.contrib.auth.views import PasswordResetView, PasswordResetConfirmView
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView, FormView
from django.contrib.auth import authenticate, login
from client.models import *
from .decorators import staff_required
from .forms import RegisterForm, ProductCrispyForm, AddPizzaCrispyForm, IngredientiForm, PersonaleForm
from django.contrib import messages
from datetime import date

from .models import Ingredienti, Personale


class PasswordReset(PasswordResetView):
    #success_url = reverse_lazy('password_reset_done')
    success_url = reverse_lazy('user_management:password_reset_done')


class PasswordResetConfirm(PasswordResetConfirmView):
    #success_url = reverse_lazy('password_reset_done')
    success_url = reverse_lazy('user_management:password_reset_complete')


class UserCreateView(CreateView):
    #form_class = UserCreationForm
    form_class = RegisterForm
    template_name = 'registration/user_create.html'
    success_url = reverse_lazy('homepage')

    def post(self, request):
        form = RegisterForm(request.POST)
        return self.form_valid(form)

"""
    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('homepage')"""

################################## super-user ##################################
@staff_required
def dashborad(request):
    return render(request, 'dashboardmenu.html')

@method_decorator(login_required, name='dispatch')
class ProductAdd(SuccessMessageMixin,CreateView):
    model = Product
    template_name = 'product/add.html'
    form_class = ProductCrispyForm
    # fields = ('first_name', 'middle_name', 'last_name')
    success_url = reverse_lazy('user_management:product-view')
    #success_message =  messages.success(request, 'Aggiunto al carrello!')

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return messages.success(self.request, 'Aggiunto al carrello!')


class ProductUpdate(SuccessMessageMixin,UpdateView, LoginRequiredMixin):
    model = Product
    template_name = 'product/update.html'
    form_class = ProductCrispyForm
    # fields = ('ProductCrispyForm', 'middle_name', 'last_name')
    success_url = reverse_lazy('user_management:product-view')

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return messages.success(self.request, 'Update fatto con carrello!')


class ProductDelete(SuccessMessageMixin,DeleteView):
    model = Product
    template_name = 'product/delete.html'
    success_url = reverse_lazy('user_management:product-view')

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return messages.success(self.request, 'Delete fatto con  success!')


class ProductView(ListView):
    model = Product
    template_name = 'product/view.html'


class BookingView(ListView):
    model = Booking
    template_name = 'booking/view.html'

    def get_queryset(self):
        queryset = Booking.objects.filter(date_start__gte=timezone.now())
        return queryset


class AddedThePizzaAdd(SuccessMessageMixin,CreateView):
    model = AddedThePizza
    template_name = 'product/add.html'
    form_class = AddPizzaCrispyForm
    # fields = ('first_name', 'middle_name', 'last_name')
    success_url = reverse_lazy('user_management:addpizza-view')
    #success_message =  messages.success(request, 'Aggiunto al carrello!')

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return messages.success(self.request, 'Aggiunto al carrello!')


class AddedThePizzaUpdate(SuccessMessageMixin,UpdateView, LoginRequiredMixin):
    model = AddedThePizza
    template_name = 'product/update.html'
    form_class = AddPizzaCrispyForm
    # fields = ('ProductCrispyForm', 'middle_name', 'last_name')
    success_url = reverse_lazy('user_management:addpizza-view')

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return messages.success(self.request, 'Update fatto con carrello!')


class AddedThePizzaDelete(SuccessMessageMixin,DeleteView):
    model = AddedThePizza
    template_name = 'product/delete.html'
    success_url = reverse_lazy('user_management:addpizza-view')

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return messages.success(self.request, 'Delete fatto con  success!')


class AddedThePizzaView(ListView):
    model = AddedThePizza
    template_name = 'addpizza/view.html'


def grafico(request):
    now  = datetime.datetime.now()
    start_date = now.strftime("%m")
    print(start_date)
    items = {}
    for x in Order.objects.filter(date_ordered__month=start_date, complete=True):
        items[int(x.date_ordered.strftime("%d"))] = 0
        items[int(x.date_ordered.strftime("%d"))] = OrderItem.objects.filter(order=x).count() + items[int(x.date_ordered.strftime("%d"))]
    list = [(k, v) for k, v in items.items()]
    print(items)
    context = {'items': list}
    return render(request, 'grafico.html', context)



class IngredientiAdd(SuccessMessageMixin,CreateView):
    model = Ingredienti
    template_name = 'ingredienti/add.html'
    form_class = IngredientiForm
    # fields = ('first_name', 'middle_name', 'last_name')
    success_url = reverse_lazy('user_management:ingredienti-view')
    #success_message =  messages.success(request, 'Aggiunto al carrello!')

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return messages.success(self.request, 'Aggiunto al carrello!')


class IngredientiUpdate(SuccessMessageMixin,UpdateView, LoginRequiredMixin):
    model = Ingredienti
    template_name = 'ingredienti/update.html'
    form_class = IngredientiForm
    # fields = ('ProductCrispyForm', 'middle_name', 'last_name')
    success_url = reverse_lazy('user_management:ingredienti-view')

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return messages.success(self.request, 'Update fatto con carrello!')


class IngredientiDelete(SuccessMessageMixin,DeleteView):
    model = Ingredienti
    template_name = 'ingredienti/delete.html'
    success_url = reverse_lazy('user_management:ingredienti-view')

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return messages.success(self.request, 'Delete fatto con  success!')


class IngredientiView(ListView):
    model = Ingredienti
    template_name = 'ingredienti/view.html'




#############################person

class PersonAdd(SuccessMessageMixin,CreateView):
    model = Personale
    template_name = 'person/add.html'
    form_class = PersonaleForm
    # fields = ('first_name', 'middle_name', 'last_name')
    success_url = reverse_lazy('user_management:person-view')
    #success_message =  messages.success(request, 'Aggiunto al carrello!')

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return messages.success(self.request, 'Aggiunto al carrello!')


class PersonUpdate(SuccessMessageMixin,UpdateView, LoginRequiredMixin):
    model = Personale
    template_name = 'person/update.html'
    form_class = PersonaleForm
    # fields = ('ProductCrispyForm', 'middle_name', 'last_name')
    success_url = reverse_lazy('user_management:person-view')

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return messages.success(self.request, 'Update fatto con carrello!')


class PersonDelete(SuccessMessageMixin,DeleteView):
    model = Personale
    template_name = 'person/delete.html'
    success_url = reverse_lazy('user_management:person-view')

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return messages.success(self.request, 'Delete fatto con  success!')


class PersonView(ListView):
    model = Personale
    template_name = 'person/view.html'
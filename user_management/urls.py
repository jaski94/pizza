from django.urls import path
from django.contrib.auth import views as auth_views
from user_management import views
from django.conf.urls.static import static
from django.conf import settings
#from django.contrib.auth.urls

app_name = 'user_management'

urlpatterns = [
    path('dashborad', views.dashborad, name='dashborad'),
    path('user/reg', views.UserCreateView.as_view(), name='user-register'),
    #django default view
    path('login', auth_views.LoginView.as_view(), name='login'),
    path('logout', auth_views.LogoutView.as_view(), name='logout'),

    path('password_reset/', views.PasswordReset.as_view(), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', views.PasswordResetConfirm.as_view(), name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    #product
    path('product/add', views.ProductAdd.as_view(), name='product-add'),
    path('product/<int:pk>/delete', views.ProductDelete.as_view(), name='product-delete'),
    path('product/<int:pk>/update', views.ProductUpdate.as_view(), name='product-update'),
    path('product/view', views.ProductView.as_view(), name='product-view'),
    #ingredienti
    path('ingredienti/add', views.IngredientiAdd.as_view(), name='ingredienti-add'),
    path('ingredienti/<int:pk>/delete', views.IngredientiDelete.as_view(), name='ingredienti-delete'),
    path('ingredienti/<int:pk>/update', views.IngredientiUpdate.as_view(), name='ingredienti-update'),
    path('ingredienti/view', views.IngredientiView.as_view(), name='ingredienti-view'),
    #addpizza
    path('addpizza/add', views.AddedThePizzaAdd.as_view(), name='addpizza-add'),
    path('addpizza/<int:pk>/delete', views.AddedThePizzaDelete.as_view(), name='addpizza-delete'),
    path('addpizza/<int:pk>/update', views.AddedThePizzaUpdate.as_view(), name='addpizza-update'),
    path('addpizza/view', views.AddedThePizzaView.as_view(), name='addpizza-view'),
    #person
    path('person/add', views.PersonAdd.as_view(), name='person-add'),
    path('person/<int:pk>/delete', views.PersonDelete.as_view(), name='person-delete'),
    path('person/<int:pk>/update', views.PersonUpdate.as_view(), name='person-update'),
    path('person/view', views.PersonView.as_view(), name='person-view'),

    path('booking/view', views.BookingView.as_view(), name='booking-view'),
    path('grafico', views.grafico, name='grafico-view'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

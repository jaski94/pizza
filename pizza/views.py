from django.shortcuts import render, redirect

"""
def homepage(request):
    return render(request, 'home.html')
"""

def homepage(request):
    if request.user.is_authenticated:
        if request.user.is_staff:
            return redirect('user_management:dashborad')
        else:
            #return render(request, 'main.html')
            return redirect('client:search')

    #return render(request, 'main.html')
    return redirect('client:search')
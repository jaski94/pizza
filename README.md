#Pizza house

L’applicazione che ho sviluppato riguarda la gestione di una pizzeria. 
L’applicazione è pensata per essere utilizzata da utenti non registrati, utenti registrati e super utenti.

# La Configurazione per il progetto

Nella cartella del progetto e assicurati di avere **virtual env**  altrimenti via su questo sito per la configurazione:
> https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/

Per controllare la versione del python

> python3 -m pip --version

Per installare tutti i packages necessari all'applicazione(compresso django), assicurati di essere
nella directory contenente il file requirements.txt e digita:

> pip install -r requirements.txt

e controllare se sono presenti tutti i pacchetti con il comando:
>  pip freeze

## Esecuzione del web applicazione 

Esguire prima il comando nella shell:
> python manage.py migrate

Per lanciare il web server che ospiterà l'applicazione, esegui:
> python manage.py runserver

## il test

Esguire il comando per il test:
> python manage.py test client.tests

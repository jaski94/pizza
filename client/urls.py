from django.urls import path

from django.conf.urls.static import static
from django.conf import settings
from . import views

# python manage.py createsuperuser
app_name = 'client'

urlpatterns = [
	#Leave as empty string for base url
	path('', views.store, name="store"),
	path('<int:id>', views.store_eater, name="eater"),
	path('category/<int:id>', views.store_category, name="category"),
	path('view/<int:id>', views.detail_iteam, name="detail"),

	path('cart/', views.cart, name="cart"),
	path('checkout/', views.checkout, name="checkout"),

	path('booking/', views.booking, name="booking"),
	path('booking/<int:pk>/delete', views.BookingDelete.as_view(), name="booking-delete"),
    path('cart/<int:pk>/delete', views.CartDelete.as_view(), name='cart-delete'),
	# ajax
	path('norm', views.normal_order, name="nornal"),
	path('camment', views.normal_comment, name="comment"),

    path('profile', views.profile_view, name='profile'),
    path('search', views.search, name='search'),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



"""	
	path('checkout/', views.checkout, name="checkout"),

	path('update_item/', views.updateItem, name="update_item"),
	path('process_order/', views.processOrder, name="process_order"),"""
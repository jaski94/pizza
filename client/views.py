from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Q
from django.shortcuts import render, redirect
from django.http import JsonResponse
import json
import datetime
from django.urls import reverse_lazy
from django.views.generic import DeleteView
from .models import *
from .forms import *
#from .utils import cookieCart, cartData, guestOrder

from django.core.mail import send_mail
from django.conf import settings
from django.contrib import messages


def emaail_local(subject, message, email_from, recipient_list):
    print("##########################################################################")
    print("\n\n\nSubject : "+subject)
    print("Email_from : "+ email_from)
    print("Message : "+ message)
    print("Recipient_list : "+ recipient_list+"\n\n\n")
    print("##########################################################################")


def store(request):
    form2 = None
    products = None
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data.get('search'))
            products = Product.objects.filter(Q(name__contains=form.cleaned_data.get('search')) |
                                              Q(description__contains=form.cleaned_data.get('search')) |
                                              Q(type_eater__contains=form.cleaned_data.get('search')) ).order_by('name')

            context = {'products': products}
            return render(request, 'product/product.html', context)

    elif request.method == 'GET' and request.GET.get('tank', False) :
        print(request.GET.get('tank'))
        if request.GET.get('tank') == '1':
            products = Product.objects.all().order_by('name')
        elif request.GET.get('tank') == '2' :
            products = Product.objects.all().order_by('-name')
        elif request.GET.get('tank') == '3' :
            products = Product.objects.all().order_by('price')
        else:
            products = Product.objects.all().order_by('-price')


        form = SearchForm()
        form2 = ProductOrderForm()
    else:
        products = Product.objects.all().order_by('price')
        form = SearchForm()
        form2 = ProductOrderForm()

    context = {'form':form, 'form2':form2,'products': products }
    return render(request, 'product/product.html', context)


def store_eater(request,id):
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data.get('search'))
            products = Product.objects.filter(Q(name__contains=form.cleaned_data.get('search')) |
                                              Q(description__contains=form.cleaned_data.get('search')) |
                                              Q(type_eater__contains=form.cleaned_data.get('search')) ).order_by('name')
            context = {'products': products}
            return render(request, 'product/product.html', context)
    else:
        if id == 1:
            products = Product.objects.filter(type_eater="vegan")
        elif id == 2:
            products = Product.objects.filter(type_eater="vegetarian")
        else:
            products = Product.objects.filter(type_eater="carnivore")
        form = SearchForm()

    context = {'form':form, 'products': products }
    return render(request, 'product/product.html', context)


def store_category(request,id):
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data.get('search'))
            products = Product.objects.filter(Q(name__contains=form.cleaned_data.get('search')) |
                                              Q(description__contains=form.cleaned_data.get('search')) |
                                              Q(type_eater__contains=form.cleaned_data.get('search')) ).order_by('name')
            context = {'products': products}
            return render(request, 'product/product.html', context)
    else:
        if id == 1:
            products = Product.objects.filter(type_product="other")
        elif id == 2:
            products = Product.objects.filter(type_product="drink")
        else:
            products = Product.objects.filter(type_product="sweet")
        form = SearchForm()

    context = {'form':form, 'products': products }
    return render(request, 'product/product.html', context)


# http://127.0.0.1:8000/accounts/login?next=/iteam/view/2
def normal_order(request):
    #product = Product.objects.get(id=id)
    #print(request.POST.get('username'))
    data = { 'is_taken': False }
    if request.method == 'POST' and request.user.is_authenticated:
        data = { 'is_taken': True }
        customer = request.user
        product = Product.objects.get(id=request.POST.get('username'))
        order,created = Order.objects.get_or_create(customer=customer, complete=False)
        adpizza = AddedThePizza.objects.get(name="niente")
        orde = OrderItem.objects.create(product=product,order=order,size="normale",quantity=1,date_added=datetime.today().strftime('%Y-%m-%d'),addpizza=adpizza)
        orde.save()
        return JsonResponse(data)
    return JsonResponse(data)

#ajax
def normal_comment(request):
    data = { 'is_taken': False }
    if request.method == 'POST' and request.user.is_authenticated:
        data = { 'is_taken': True }

        product = Product.objects.get(id=request.POST.get('product'))
        com = Comment.objects.create(comment=request.POST.get('comment'), rate=request.POST.get('rate'),product=product, customer=request.user)
        com.save()
        return JsonResponse(data)
    return JsonResponse(data)


# AJAX non si puo mandare varile url template con int id in ajax likeeeeee {% url 'client:detail' product.id %}
# AJAX pero si possono fare degli escamotage(----with trick---) mettere url in un attributo e dopo con le funzioni javascipt o jquery
# AJAX O COME HO FATTO IO SOPRA
@login_required(login_url='/accounts/login')
def detail_iteam(request, id):
    rate = 0
    media_star = 0
    comment = []
    count = 0
    product = Product.objects.get(id=id)
    products2 = []
    ycomment=[]
    #print(product.type_eater)
    if request.method == 'POST':
        customer = request.user
        form = ProductDetailForm(request.POST)

        if form.is_valid():
            print(form.cleaned_data.get('side_dish'))
            order,created = Order.objects.get_or_create(customer=customer, complete=False)
            orderItem = form.save(commit=False)
            orderItem.product = product
            orderItem.order = order
            orderItem.addpizza = AddedThePizza.objects.get(id=form.cleaned_data.get('side_dish')) # devo aggiungere 0 default 0
            orderItem.date_added = datetime.today().strftime('%Y-%m-%d')
            orderItem.save()
            messages.success(request, 'Aggiunto al carrello!')
        else:
            messages.error(request, 'Salvataggio non ha avuto successo!')

        return redirect('client:store')
    else:
        form = ProductDetailForm()
        comment = Comment.objects.filter(product=product)
        ycomment = Comment.objects.filter(Q(product__type_eater=product.type_eater), ~Q(product=product))
        ycomment2 = ycomment.values_list('product', flat=True)
        try:
            products2 = Product.objects.filter(Q(type_eater=product.type_eater), ~Q(id=id), ~Q(id__in=ycomment2))
            #products2 = Product.objects.filter(Q(type_eater=product.type_eater), ~Q(id=id))
        except Order.DoesNotExist:
            products2 = []

        print(ycomment)
        print(products2)
        for x in comment:
            rate = x.rate + rate
        count = Comment.objects.filter(product=product).count()
        try:
            media_star =  (rate / count)
        except ZeroDivisionError:
            media_star = 0
        #form2 = CommentForm()

    context = {'product': product, 'products2': products2, 'ycomment': ycomment,'form':form, 'media_star':media_star, 'comment':comment, 'count':count}
    return render(request, 'product/product_view.html', context)


# ancora da sistemare
@login_required(login_url='/accounts/login')
def checkout(request):
    items = None
    order = None
    customer = request.user
    if request.method == 'POST':
        form = ShipForm(request.POST)
        if form.is_valid():
            form.save()
            complete_rder = Order.objects.get(customer=customer, complete=False)
            complete_rder.complete=True
            complete_rder.date_ordered=timezone.now()
            complete_rder.save()
            # eamil
            subject = 'Thank you for checkout to our site'
            message = "see you next time. date order : " + str(complete_rder.date_ordered)
            email_from = settings.EMAIL_HOST_USER
            recipient_list = [complete_rder.customer.email]
            #recipient_list = ['jaski294@gmail.com', ]
            #send_mail( subject, message, email_from, recipient_list )
            emaail_local( subject, message, email_from, str(recipient_list) )

        return redirect('client:profile')

    else:
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        items = order.orderitem_set.all()

    form = ShipForm()
    context = {'form':form,'items':items, 'order':order}
    return render(request, 'cart/checkout.html', context)


class CartDelete(DeleteView):
    model = OrderItem
    template_name = 'cart/delete.html'
    success_url = reverse_lazy('client:cart')


def cart(request):
    # request.user.is_authenticated
    if request.user.is_authenticated:
        customer = request.user
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        items = OrderItem.objects.filter(order=order)
    else:
        items = []
        order = {'get_cart_total':0, 'get_cart_items':0}

    context = {'items':items, 'order':order}
    return render(request, 'cart/cart.html', context)


@login_required(login_url='/accounts/login')
def booking(request):
    if request.method == 'POST' :
        customer = request.user
        form = BookingForm(request.POST)
        print(str(request.POST.get('date_start')).split("/"))
        a = str(request.POST.get('date_start')).split(" ")
        b = a[1].split(":")
        a = a[0].split("/")
        print(a,b)
        object_list = Booking.objects.filter(date_start__day=a[0], date_start__month=a[1], date_start__year=a[2])
        #datetime.date(2005, 1, 3)
        print(sum(object_list.values_list('chair', flat=True)))
        if form.is_valid() and sum(object_list.values_list('chair', flat=True)) < 40:
            booking = form.save(commit=False)
            booking.customer = customer
            booking.save()

            #object_list = Booking.objects.filter(customer=request.user, date_start__gte=timezone.now())
            # eamil
            subject = 'Thank you for Booking to our site'
            message = booking.name + "\n" + "booking start :"+ str(booking.date_start) + "booking end :" + str(booking.date_end)
            email_from = settings.EMAIL_HOST_USER
            recipient_list = [booking.customer.email,]
            #recipient_list = ['jaski294@gmail.com', ]
            #send_mail( subject, message, email_from, recipient_list )
            emaail_local( subject, message, email_from, str(recipient_list) )
            messages.success(request, 'Salvataggio ha avuto successo!')
        else:
            messages.error(request, 'Salvataggio non ha avuto successo! o non sono disponibili i posti')
        #order = Order.objects.get(customer=customer, complete=False)
        #items = OrderItem.objects.filter(order=order)
    else:
        form = BookingForm()

    context = {'form':form }
    return render(request, 'booking/booking.html', context)


class BookingDelete(SuccessMessageMixin,DeleteView):
    model = Booking
    template_name = 'booking/delete.html'
    success_url = reverse_lazy('client:profile')

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return messages.success(self.request, 'Delete fatto con  success!')


@login_required(login_url='/accounts/login')
def profile_view(request):
    """
        Funzione che si occupa della visualizzazione del profilo utente e gestione degli termini della balcklist e
        cancellazione degli articoli

    """

    object_list = Booking.objects.filter(customer=request.user,date_start__gte=timezone.now())
    #object_list = Booking.objects.filter(customer=request.user)
    try:
        order_false = Order.objects.get(customer=request.user, complete=False)
        order_items_false = OrderItem.objects.filter(order=order_false)
    except Order.DoesNotExist:
        order_items_false = []

    try:
        order_true = Order.objects.filter(customer=request.user, complete=True)
        order_items_true = OrderItem.objects.filter(order__in=order_true)
    except Order.DoesNotExist:
        order_items_true = []

    context = {'object_list': object_list,'order_items_false': order_items_false,'order_items_true': order_items_true}
    return render(request, 'profile.html', context)


def search(request):
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data.get('search'))
            products = Product.objects.filter(Q(name__contains=form.cleaned_data.get('search')) | Q(description__contains=form.cleaned_data.get('search')) | Q(type_eater__contains=form.cleaned_data.get('search')) )

            context = {'products': products}
            return render(request, 'product/product.html', context)
    else:
        form = SearchForm()

    context = {'form':form }
    return render(request, 'search.html', context)


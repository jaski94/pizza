from django.contrib import admin
from .models import  Product,Order,ShippingAddress, OrderItem, Booking, AddedThePizza, Comment
# Register your models here.

admin.site.register(Product)
admin.site.register(Order)
admin.site.register(ShippingAddress)
admin.site.register(OrderItem)
admin.site.register(Booking)
admin.site.register(AddedThePizza)
admin.site.register(Comment)
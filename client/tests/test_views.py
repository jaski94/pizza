from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.contrib.auth.models import User
from client.models import *

class Test_Client_ViewsC(TestCase):

    #Istanzio quanto comune ed il cliente virtuale
    def setUp(self):
        self.client = Client()

    def test_register_load_page(self):
        response = self.client.get(reverse('client:store'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'product/product.html')
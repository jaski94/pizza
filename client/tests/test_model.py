from django.test import TestCase
from django.utils import timezone

from client.models import Product, OrderItem, Order, AddedThePizza
from user_management.models import User

class Test_Client_Models(TestCase):

    def setUp(self):
        self.user_test = User.objects.create(email='test@test.it',first_name='test_user',last_name='test_last')
        self.order_test = Order.objects.create(customer=self.user_test,complete=False)
        self.addpizza_test = AddedThePizza.objects.create(name="patainte_test",price=4.0)
        self.product_test = Product.objects.create(name="pizza rossa",description="sugo con sale e aglio",type_eater="Omnivore",type_product="pizza",price=4.0)

    def test_product_fields(self):
        product_test = Product()
        product_test.name = "pizza rossa"
        product_test.description = "sugo con sale e aglio"
        product_test.type_eater = "Omnivore"
        product_test.type_product = "pizza"
        product_test.price = 4.0
        product_test.image = "pizza_prova_5.jpg"
        product_test.save()

        record = Product.objects.get(id=product_test.id)
        self.assertEqual(record, product_test)

    def test_order_item_fields(self):
        order_item_test = OrderItem()
        order_item_test.product = self.product_test
        order_item_test.order = self.order_test
        order_item_test.addpizza = self.addpizza_test
        order_item_test.size = "normale"
        order_item_test.quantity = 1
        order_item_test.save()

        record = OrderItem.objects.get(id=order_item_test.id)
        self.assertEqual(record, order_item_test)

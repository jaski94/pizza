from django.test import TestCase
from client.forms import BookingForm

#python manage.py test client.tests
class Test_Client_Forms(TestCase):

    """
        Un test viene eseguendo un set di funzionalità per la nostra applicazione.per verificare Il corretto sviluppo
        dei casi di test trova problemi nella funzionalità di un'applicazione.
        con form test servono per vedere se i dati  messi negli fields sono giusti o se ci sono dei problemi
    """

    # Valid Form Data
    def test_bookingforms_valid(self):
        form = BookingForm(data={'name': "test", 'description': "la stampa", 'chair': 2, 'date_start': "22/08/2020 12:33","date_end":"22/08/2020 12:43"})
        self.assertTrue(form.is_valid())

    # Invalid Form Data
    def test_bookingforms_invalid(self):
        form = BookingForm(data={'name': "test333", 'description': "la stampa", 'chair': 2})
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 2)
import email
from datetime import datetime

# import self as self
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms

from .models import ShippingAddress, OrderItem, Booking, AddedThePizza, Comment
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, ReadOnlyPasswordHashField
from django.contrib.auth import authenticate, login



class ProductDetailForm(forms.ModelForm):
    """
        form per articolo con i diversi field e insieme a crispy
    """
    def __init__(self, *args, **kwargs):
        """if AddedThePizza.DoesNotExist or False  :
            alll = AddedThePizza.objects.all()
        else:
            alll = []
            """
        super(ProductDetailForm, self).__init__(*args, **kwargs)
        alll = AddedThePizza.objects.all()
        id_list = [foo.id for foo in alll]
        name_list = [foo.name for foo in alll]
        ADDPIZZA_CHOICES = list(zip(id_list, name_list))
        self.fields['side_dish']  = forms.CharField(initial=0,widget=forms.Select(choices=ADDPIZZA_CHOICES))

    quantity = forms.IntegerField(min_value=1, max_value=10, initial=1 , label="Quntita:")
    class Meta:
        model = OrderItem
        fields = ('size', 'quantity')



class BookingForm(forms.ModelForm):
    """
        form per articolo con i diversi field e insieme a crispy
    """
    chair = forms.IntegerField(initial=1,min_value=1,max_value=10)
    date_start = forms.DateTimeField( input_formats=['%d/%m/%Y %H:%M'], widget=forms.DateTimeInput(attrs={ 'class': 'form-control datetimepicker-input','data-target': '#datetimepicker1'}))
    date_end = forms.DateTimeField( input_formats=['%d/%m/%Y %H:%M'], widget=forms.DateTimeInput(attrs={ 'class': 'form-control datetimepicker-input','data-target': '#datetimepicker1'}))
    class Meta:
        model = Booking
        fields = ('name','description', 'chair','date_start', 'date_end')


class CommentForm(forms.ModelForm):
    """
        form per articolo con i diversi field e insieme a crispy
    """
    rate = forms.IntegerField(initial=1,min_value=1,max_value=5)
    class Meta:
        model = Comment
        fields = ('comment','rate')


class SearchForm(forms.Form):
    """
        form per articolo con i diversi field e insieme a crispy
    """
    search = forms.CharField(required=False)


class ShipForm(forms.ModelForm):
    #n_cart = forms.IntegerField(min_value=0, max_value=3, initial=0 , label="Non analizzare i termini con un numero di lettere inferiore a:", widget=forms.Select(choices=CHARACTERS_CHOICES))
    #data = forms.DateField(label="Data (AAAA-MM-GG)")
    class Meta:
        model = ShippingAddress
        fields = ('address', 'city', 'state', 'zipcode', 'card_pay')




class ProductOrderForm(forms.Form):
    choices = [(0, 'not'),(1, 'ascending name'), (2, 'descending name'), (3, 'ascending price'),  (4, 'descending price')]
    #tank = forms.ChoiceField(choices=choices,  widget=forms.Select(attrs={'onchange': 'actionform.submit();'}))
    tank = forms.ChoiceField(choices=choices,  widget=forms.Select(attrs={"onChange":'submit()'}))
    #tank  = forms.CharField(initial=0, required=False,widget=forms.Select(choices=CHARACTERS_CHOICES))

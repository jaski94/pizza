from django.db import models
#from django.contrib.auth.models import User
# Create your models here.
from django.utils import timezone
from pizza.settings import AUTH_USER_MODEL as User
## https://www.google.com/search?q=type+of+vegan+vegetarian+or+meat&tbm=isch&ved=2ahUKEwialZfn0qnrAhUSaBoKHSgvDocQ2-cCegQIABAA&oq=type+of+vegan+vegetarian+or+meat&gs_lcp=CgNpbWcQAzoECAAQQzoCCAA6BQgAELEDOgQIABATOgQIABAeULTMBVjsrgZg0K8GaABwAHgAgAHIAYgBuBeSAQY2LjIyLjSYAQCgAQGqAQtnd3Mtd2l6LWltZ8ABAQ&sclient=img&ei=RFo-X5rjNpLQaajeuLgI&bih=951&biw=1853&client=firefox-b-d#imgrc=snrGDYeF0EkiuM&imgdii=JynMuMmCgcai1M


class AddedThePizza(models.Model):
    name = models.CharField(max_length=100)
    price = models.FloatField()


class Product(models.Model):
    SIZE_CHOICES = [
        ('vegan', 'vegan'),
        ('vegetarian', 'vegetarian'),
        ('carnivore', 'carnivore'),
        ('Omnivore', 'Omnivore'),
    ]
    SIZE_CHOICES2 = [
        ('pizza', 'pizza'),
        ('sweet', 'sweet'),
        ('other', 'other'),
        ('drink', 'drink'),
    ]
    name = models.CharField(max_length=200)
    type_product = models.CharField(max_length=200,null=True,choices=SIZE_CHOICES2)
    type_eater = models.CharField(max_length=200,null=True,choices=SIZE_CHOICES) # https://themissinggraph.wordpress.com/2011/04/11/we-are-what-we-eat-an-infographic/
    price = models.FloatField()
    description = models.TextField(null=True)
    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.name


class Order(models.Model):
    customer = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    #product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    date_ordered = models.DateTimeField(default=timezone.now, null=True)
    complete = models.BooleanField(default=False)

    def __str__(self):
        return str(self.id)

    @property
    def shipping(self):
        shipping = False
        orderitems = self.orderitem_set.all()
        for i in orderitems:
            if i.product.digital == False:
                shipping = True
        return shipping

    @property
    def get_cart_total(self):
        orderitems = self.orderitem_set.all()
        total = sum([item.get_total for item in orderitems])
        return total

    @property
    def get_cart_items(self):
        orderitems = self.orderitem_set.all()
        total = sum([item.quantity for item in orderitems])
        return total



class OrderItem(models.Model):
    SIZE_CHOICES = [
        ('normale', 'normale'),
        ('piccolo', 'piccolo'),
        ('grande', 'grande'),
        ('mezzo metro', 'mezzo metro'),
    ]
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)
    addpizza = models.ForeignKey(AddedThePizza, on_delete=models.CASCADE, null=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, null=True)
    size = models.CharField(max_length=50,choices=SIZE_CHOICES, default="normale")
    quantity = models.IntegerField(default=1, null=True, blank=True)
    # aggiunte (verdure carne e altro)
    date_added = models.DateTimeField(default=timezone.now, null=True, blank=True)

    @property
    def get_total(self):
        """if self.addpizza:
            add_pizza = self.addpizza.price
        else:
            add_pizza = 0"""

        total = (self.product.price * self.quantity) + (self.addpizza.price * self.quantity)
        return total


class Booking(models.Model):
    customer = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=200, null=True,blank=True)
    description = models.TextField(null=True, blank=True)
    chair = models.IntegerField(default=1, null=True, blank=True)
    date_start = models.DateTimeField(default=timezone.now, null=True, blank=True)
    date_end = models.DateTimeField(default=timezone.now, null=True, blank=True)

    def __str__(self):
        return self.name

class Comment(models.Model):
    customer = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)
    comment = models.TextField(null=True, blank=True)
    rate = models.FloatField(default=1, null=True, blank=True)

    def __str__(self):
        return self.comment



class ShippingAddress(models.Model):
    customer = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, null=True)
    address = models.CharField(max_length=200, null=False)
    city = models.CharField(max_length=200, null=False)
    state = models.CharField(max_length=200, null=False)
    zipcode = models.CharField(max_length=5, null=False)
    card_pay = models.CharField(max_length=10, null=False)
    date_added = models.DateTimeField(default=timezone.now, null=True, blank=True)

    def __str__(self):
        return self.address